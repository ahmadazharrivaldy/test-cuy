how to use:
```shell
helm upgrade --install x509-certificate-exporter enix/x509-certificate-exporter -f values.yaml -n ops-monit
```

then add job on your prometheus cm:
```yaml
- job_name: 'x509-exporter'
        scrape_interval: 5s
        static_configs:
          - targets: ['x509-certificate-exporter-headless.ops-monit.svc.cluster.local:9793']
```

## sources:
- **EDM Production Production**