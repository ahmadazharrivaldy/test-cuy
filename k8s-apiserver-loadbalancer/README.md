# Haproxy LoadBalancer K8s ApiServer

> Untuk kubernetes yang mempunyai lebih dari satu master

### How to use,

> Exec on all master node

1. Install Haproxy di masing-masing master node

```bash
sudo apt install haproxy
```

2. Backup konfigurasi

```bash
sudo cp haproxy.cfg haproxy.cfg.bak
```

3. Sesuaikan manifest dengan `haproxy.cfg`


Source,

- EDM Prod Calico

# Nginx

> exec on instance loadbalancer

1. Install nginx

```bash
sudo apt install nginx
```

2. Remove default config dari nginx

```bash 
sudo rm /etc/nginx/sites-enabled/default
```

3.  Backup config

```bash
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
```

4. Sesuaikan konfigurasi dengan `nginx.conf`

5. Cek konfigurasi

```bash
sudo nginx -t
```

6. Reload nginx

```bash
sudo systemctl reload nginx
```