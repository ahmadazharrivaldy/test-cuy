#!/bin/bash

PODS=$(kubectl get pod -n ops-monit | grep blackbox | awk {'print $1}')
for pod in $PODS;
do
	echo -n "reloading config $pod: "
	kubectl exec -n ops-monit $pod -c blackbox-exporter -- wget -O- --post-data "" http://localhost:9115/-/reload
	echo "done."
	echo "====="
done
