## How To Apply
> If namespace ops-monit not existing
```sh
kubectl create namespace ops-monit
```
> Deploy Blackbox
1. Create service account blackbox
```sh
kubectl create sa blackbox -n ops-monit
```
2. Apply ConfigMap.yaml
```sh
kubectl apply -f ConfigMap.yaml
```
3. Apply role.yaml
```sh
kubectl apply -f role.yaml
```
4. Apply rolebinding.yaml
```sh
kubectl apply -f rolebinding.yaml
```
5. Apply DaemonSet-hostaliases.yaml
```sh
kubectl apply -f DaemonSet-hostaliases.yaml
```
6. Apply Service.yaml
```
kubectl apply -f Service.yaml
```

## Sources
- EDM Production Cluster Manifest
- https://go.btech.id/ops/bri/-/issues/387#note_39569
