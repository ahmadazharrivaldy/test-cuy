# Ingress Loadbalancer with Haproxy

> Exec on lb node
1. Install Haproxy 

```bash
sudo apt install haproxy -y
```
2. Konfigurasi haproxy.cfg untuk loadbalancer ingress
3. Verifikasi konfigurasi haproxy
```bash
haproxy -c -V -f /etc/haproxy/haproxy.cfg
```
4. Restart Service Haproxy
```bash
sudo systemctl restart haproxy.service
```