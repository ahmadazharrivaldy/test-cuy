# Ingress Loadbalancer Nginx

> Exec on lb node

1. Install nginx

```bash
sudo apt install nginx
```

2. Bakcup `nginx.conf` existing menjadi `nginx.conf.bak` dan replace dengan `nginx.conf` pada repository ini. 

> Note: Expose metrics prometheus-nginx di manifest `nginx.conf`
> Jangan di exec ini hanya catatan

```config
http {
        server {
            listen localhost:93;
            location /metrics {
                stub_status on;
            }
        }

#...
}
```

> Note: Menambahkan stream dan mendisable include default pada `nginx.conf`
> Jangan di exec ini hanya catatan
```config
http {
	...
        # include /etc/nginx/conf.d/*.conf;
        # include /etc/nginx/sites-enabled/*;
}

stream {

    include /etc/nginx/stream.d/*.conf;

    log_format basic '$remote_addr [$time_local] '
                 '$protocol $status $bytes_sent $bytes_received '
                 '$session_time "$upstream_addr" '
                 '"$upstream_bytes_sent" "$upstream_bytes_received" "$upstream_connect_time"';

    access_log /var/log/nginx/access.log basic;
    error_log  /var/log/nginx/error.log;
}
```

3. Membuat directory `/etc/nginx/stream.d`
```sh
sudo mkdir /etc/nginx/stream.d
```

4. Menambahkan manifest `be-ingress-http-kubernetes.conf` dan `be-ingress-http-kubernetes.conf` ke `/etc/nginx/stream.d`

5. Test & reload config nginx

```bash
sudo nginx -t

# Reload nginx

sudo systemctl reload nginx
```

## Install nginx-exporter (Kebutuhan monitoring prometheus)

1. Install manifest

```bash
# Download
wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v0.11.0/nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz

# Ekstrak
tar -xf nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz

# Pindahkan program
sudo mv nginx-prometheus-exporter /usr/local/bin

# Buat user
sudo useradd -r nginx_exporter

```

2. Tambahkan manifest `nginx_prometheus_exporter.service` ke `/etc/systemd/system/`

```bash
cp nginx_prometheus_exporter.service /etc/systemd/system/
```

3. Reload & restart nginx-prometheus-exporter

```bash
sudo systemctl daemon-reload
sudo systemctl restart nginx_prometheus_exporter
sudo systemctl status nginx_prometheus_exporter

```

4. Menambah jobs di prometheus

```yaml
- job_name: 'ingress-loadbalancer'
  static_configs:
     - targets:
       - 10.40.210.15:9113
       - 10.40.210.16:9113
       - 172.18.218.100:9113

```

5. Import dashboard [Ini](https://github.com/nginxinc/nginx-prometheus-exporter/blob/main/grafana/dashboard.json) ke grafana


## Source

- EDM Staging Calico
