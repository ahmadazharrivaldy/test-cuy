#!/bin/bash

PODS=$(kubectl get pod -n ops-monit | grep prometheus-deployment | awk {'print $1}')
for pod in $PODS;
do
        echo -n "reloading config $pod: "
        kubectl exec -n ops-monit $pod -c prometheus -- wget -O- --post-data "" http://localhost:9090/-/reload
        echo "done."
        echo "====="
done
