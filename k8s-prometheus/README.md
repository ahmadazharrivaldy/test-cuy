# How to apply

1. Perhatikan dan cek kembali file configmap seperti target haproxy, ingress, dan ingress loadbalancer disesuaikan terlebih dahulu
2. File `pv-pvc-prometheus.yaml` menggunakan storage class manual hostpath, bisa desuaikan kembali jika menggunakan storage lain (ex. NFS )
3. Jika semua sudah sesuai, apply manifest dengan `kubectl apply -f .`
4. Jika ada perubahan atau edit pada configmap bisa direload dengan menggunakan script `./reload-cm.sh`

# Source

**Cluster EDM - Staging Calico**
